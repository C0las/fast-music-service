var gulp = require("gulp");
const sass = require("gulp-sass");
var nodemon = require("gulp-nodemon");
var browserSync = require("browser-sync");

// Task Start Server
gulp.task(
  "start-dev",
  gulp.series((cb) => {
    var called = false;
    return nodemon({
      script: "src/bin/www",
      ext: "js css .hbs",
      env: { NODE_ENV: "development" },
    });
  })
);

// Tarea de ejecucion Sass
gulp.task(
  "sass",
  gulp.series(() => {
    return gulp
      .src([
        "node_modules/bootstrap/scss/bootstrap.scss",
        "node_modules/@fortawesome/fontawesome-free/scss/fontawesome.scss",
        "src/public/scss/*.scss",
      ])
      .pipe(sass({ outputStyle: "compressed" })) // codigo minificado
      .pipe(gulp.dest("src/public/css")); // los archivos procesados se alojaran ahi
  })
);

gulp.task(
  "js",
  gulp.series(() => {
    return gulp
      .src([
        "node_modules/bootstrap/dist/js/bootstrap.min.js",
        "node_modules/jquery/dist/jquery.slim.min.js",
        "node_modules/@popperjs/core/dist/umd/popper.min.js",
      ])
      .pipe(gulp.dest("src/public/js"));
  })
);

/*gulp.task(
  "font-awesome",
  gulp.series(() => {
    return gulp
      .src("node_modules/@fortawesome/fontawesome-free/css/all.min.css")
      .pipe(gulp.dest("src/public/css"));
  })
);*/

gulp.task(
  "fonts",
  gulp.series(() => {
    return gulp
      .src("node_modules/@fortawesome/fontawesome-free/webfonts/*")
      .pipe(gulp.dest("src/public/fonts"));
  })
);

// Actualiza los cambios de la carpeta public y bootstramp
gulp.watch(
  ["node_modules/bootstrap/scss/bootstrap.scss", "src/public/scss/*.scss"],
  gulp.parallel(["sass"])
);

// Actualiza los cambios de la carpeta views
gulp.watch([
  "/src/views/*.hbs",
  "/src/views/layouts/*.hbs",
  "/src/views/partials/*.hbs",
]);

// Start Task
gulp.task(
  "default",
  gulp.series([/*"font-awesome",*/ "fonts", "js", "sass", "start-dev"])
);
