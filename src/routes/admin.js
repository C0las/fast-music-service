const { Router } = require("express");
const router = Router();

const {
  renderDashboard,
  renderProduct,
  createProduct,
  deleteProduct,
  renderOrder,
  renderClient,
} = require("../controllers/admin.controller");

const { isAuthenticated } = require("../helpers/auth");

router.get("/admin/dashboard", isAuthenticated, renderDashboard);

router.get("/admin/dashboard/product", isAuthenticated, renderProduct);

router.post("/admin/dashboard/product", isAuthenticated, createProduct);

router.delete("/admin/dashboard/product/:id", isAuthenticated, deleteProduct);

router.get("/admin/dashboard/order", isAuthenticated, renderOrder);

router.get("/admin/dashboard/order/client/:id", isAuthenticated, renderClient);

module.exports = router;
