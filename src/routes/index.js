var { Router } = require("express");
var router = Router();

var {
  renderIndex,
  renderLogin,
  renderContact,
  createOrder,
} = require("../controllers/index.controller");

/* GET home page. */
router.get("/", renderIndex);
router.get("/login", renderLogin);
router.get("/contact", renderContact);
router.post("/contact", createOrder);
module.exports = router;
