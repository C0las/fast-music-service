var indexCtrl = {};

const Order = require("../models/order");

indexCtrl.renderIndex = (req, res, next) => {
  res.render("index", { title: "Express" });
};

indexCtrl.renderLogin = (req, res, next) => {
  res.render("login", { layout: "other" });
};

indexCtrl.renderContact = (req, res, next) => {
  res.render("contact", { layout: "other" });
};

indexCtrl.createOrder = async (req, res) => {
  const errors = [];
  const {
    customerName,
    customerLastName,
    customerRut,
    customerPhone,
    customerEmail,
    country,
    address,
    street,
    city,
    comments,
  } = req.body;

  const newOrder = new Order({
    comments,
    customer: {
      customerName,
      customerLastName,
      customerRut,
      customerEmail,
      customerPhone,
      customerAddress: { address, street, city, country },
    },
  });
  await newOrder.save();
  res.redirect("/contact");
};

module.exports = indexCtrl;
