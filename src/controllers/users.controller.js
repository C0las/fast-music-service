const usersCtrl = {};
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const User = require("../models/user");

usersCtrl.renderSignUpForm = (req, res) => {
  res.render("users/signup", { layout: "other" });
};

usersCtrl.signup = async (req, res) => {
  const errors = [];
  const { name, email, password } = req.body;

  if (errors.length > 0) {
    res.render("users/signup", { layout: "other", errors, name, email });
  } else {
    const passwordUser = await User.findOne({ email: email });
    if (passwordUser) {
      req.flash("error_msg", "El correo ingresado ya esta en uso.");
      res.redirect("/users/signup");
    } else {
      const newUser = new User({ name, email, password });
      newUser.password = await newUser.encryptPassword(password);
      await newUser.save();
      res.redirect("/users/signin");
    }
  }
};

usersCtrl.renderSignInForm = (req, res) => {
  res.render("users/signin", { layout: "other" });
};

usersCtrl.signin = passport.authenticate("local", {
  failureRedirect: "/users/signin",
  successRedirect: "/admin/dashboard",
  failureFlash: true,
});

/*const errors = [];
  const { name, password } = req.body;

  if (errors.length > 0) {
    res.render("users/signin", { layout: "other", errors, name, password });
  } else {
    const passwordUser = await User.findOne({ password: password });
    const nameUser = await User.findOne({ name: name });
    if (passwordUser) {
      res.send("signin succesfuly");
    } else {
      req.flash("error_msg", "The password not exist");
      res.redirect("/users/signin");
    }
  }*/

usersCtrl.logout = (req, res) => {
  req.logout();
  req.flash("success_msg", "¡Tu sesion ha sido cerrada con exito!.");
  res.redirect("/users/signin");
};

module.exports = usersCtrl;
