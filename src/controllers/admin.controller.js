var adminCtrl = {};
const { isAuthenticated } = require("../helpers/auth");

const Product = require("../models/product");
const Order = require("../models/order");
const router = require("../routes/users");

adminCtrl.renderDashboard = async (req, res) => {
  const user = req.user;
  const orderCount = await Order.find().countDocuments();
  const orderDate = await Order.findOne().sort({ dateOrder: "asc" });
  const date = new Date(orderDate.dateOrder);
  const dateFormat = date.getHours() + " : " + date.getMinutes();
  const product = await Product.find().limit(4);
  res.render("admin/dashboard", {
    layout: "admin",
    user,
    product,
    orderCount,
    dateFormat,
  });
};

adminCtrl.renderProduct = async (req, res) => {
  const user = req.user;
  const product = await Product.find();
  res.render("admin/product", { layout: "admin", user, product });
};

adminCtrl.createProduct = async (req, res) => {
  const errors = [];
  const { productName, description, category, price, stock } = req.body;
  const newProduct = new Product({
    productName,
    description,
    category,
    price,
    stock,
  });
  await newProduct.save();
  res.redirect("/admin/dashboard/product");
};

adminCtrl.deleteProduct = async (req, res) => {
  await Product.findByIdAndDelete(req.params.id);
  res.redirect("/admin/dashboard/product");
};

adminCtrl.renderOrder = async (req, res) => {
  const user = req.user;
  const order = await Order.find();
  res.render("admin/order", { layout: "admin", user, order });
};

adminCtrl.renderClient = async (req, res) => {
  const user = req.user;
  const order = await Order.findById(req.params.id);
  res.render("admin/client_profile", { layout: "admin", user, order });
};

module.exports = adminCtrl;
