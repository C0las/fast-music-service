const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const User = require("../models/user");

passport.use(
  new LocalStrategy(
    {
      usernameField: "name",
      passwordField: "password",
    },
    async (name, password, done) => {
      // Match Name's user
      const user = await User.findOne({ name });
      if (!user) {
        return done(null, false, { message: "El usuario ingresado no existe" });
      } else {
        // Match Password's User
        const match = await user.matchPassword(password);
        if (match) {
          return done(null, user);
        } else {
          return done(null, false, { message: "Contraseña Incorrecta" });
        }
      }
    }
  )
);

// Verificar la autenticacion
passport.serializeUser((user, done) => {
  done(null, user.id);
});

// Termina la Sesison
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});
