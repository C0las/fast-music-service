var { Schema, model } = require("mongoose");
var bcrypt = require("bcryptjs");

var userShema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// Cifrar Contraseña
userShema.methods.encryptPassword = async (password) => {
  var salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

// Devuelve True o False, validacion de la contraseña del usuario
userShema.methods.matchPassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

module.exports = model("user", userShema);
