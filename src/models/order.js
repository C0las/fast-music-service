var { Schema, model } = require("mongoose");

var orderShema = new Schema({
  state: {
    type: Boolean,
    default: true,
  },
  comments: {
    type: String,
    require: true,
  },
  dateOrder: {
    type: Date,
    default: Date.now,
  },
  customer: {
    customerName: {
      type: String,
      required: true,
    },
    customerLastName: {
      type: String,
      required: true,
    },
    customerRut: {
      type: String,
      required: true,
    },
    customerEmail: {
      type: String,
      required: true,
    },
    customerPhone: {
      type: String,
      required: true,
    },
    customerAddress: {
      address: {
        type: String,
        require: true,
      },
      street: {
        type: String,
        require: true,
      },
      city: {
        type: String,
        require: true,
      },
      country: {
        type: String,
        require: true,
      },
    },
  },
});

module.exports = model("order", orderShema);
